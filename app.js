const html = `
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>大话前端</title>
</head>
<style>
  html, body, h1, ul, li{
    margin: 0;
    padding: 0;
  }
  html, body{
    height: 100%;
  }
  body{
    display: flex;
    flex-direction: column;
    background: #000;
    color: #fff;
  }
  header{
    margin-top: 100px;
  }
  h1{
      text-align: center;
      color: purple;
  }
  ul{
      display: flex;
      justify-content: center;
  }
  li{
    list-style: none;
    margin: 10px 20px;
  }
  a{
    font-size: 30px;
    text-decoration: none;
    color: #fff;
  }
  nav{
    flex: 1;
    display: flex;
    align-items: center;
    justify-content: center;
  }
  footer{
    height:60px;
    line-height: 60px;
    text-align: center;
  }
</style>
<body>
    <header>
        <h1>欢迎来到“大话前端”！正在建设中，请耐心等待。。。</h1>
    </header>
    <nav>
      <ul>
          <li><a href="">尽</a></li>
          <li><a href="">情</a></li>
          <li><a href="">期</a></li>
          <li><a href="">待</a></li>
      </ul>
    </nav>
    <footer>粤ICP备18027741号</footer>
</body>
</html>
`

const http = require('http')
http.createServer(function (req, res) {
  res.writeHead(200, {'Content-Type': 'text/html'})
  res.end(html)
}).listen(3000)
console.log('http://112.74.54.149:3000')